play_book="/etc/ansible/PlayBooks/Mounting_Oracle_File_Share.yml"
server="10.40.1.12"
destination="/u01"
data="/etc/ansible/PlayBooks/mount_oracle_file_share.txt"
while read -r line
do
node=`echo $line | cut -d',' -f1`
export=`echo $line | cut -d',' -f2`
ansible-playbook "$play_book" -e "my_host=$node" -e "remote_share=$server:$export" -e "destination=$destination"
echo "Mounted $server:$export as $destination on $node"
done < "$data"
